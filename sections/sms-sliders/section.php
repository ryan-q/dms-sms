<?php
/*
	Plugin Name: SMS Slider Section
	Author: Milo Jennings
	Author URI: http://roadsidemultimedia.com
	Description: A slider section
	Class Name: sms_slider_section
	Version: 1.0
	Filter: component, gallery, slider, dual-width
	Loading: active
	Section: true
*/

// Our class is namespaced with initials and the section name.
class sms_slider_section extends PageLinesSection {

	public function __construct(){
		parent::__construct();
		$this->name = "Slider";
	}

	// RUNS ALL TIME - This loads all the time, even if the section isn't on the page. Actions can go here, as well as post type setup functions
	function section_persistent(){
		
	}

	// LOAD SCRIPTS
	function section_scripts(){
		// wp_enqueue_script('slick-slider', 'http://cdn.jsdelivr.net/jquery.slick/1.3.7/slick.min.js', array('jquery'), '1.3.7', true );
		// wp_enqueue_script('slick-slider', '//cdn.jsdelivr.net/jquery.slick/latest/slick.min.js', array('jquery'), false, true );
		wp_enqueue_script('slick-slider', DMS_SMS_URL .'/assets/js/slick.min.js', array('jquery'), false, true );
	}

	// RUNS IN <HEAD>
	function section_head() {

		// Always use jQuery and never $ to avoid issues with other store products

			/*
		?><script>
			jQuery(document).ready(function($){

			});
		</script><?php
		*/

	}

		// BEFORE SECTION - This adds a class to the section wrap. You can also put HTML here and it will run outside of the section, and before it.
	function before_section_template( $location = '', $clone_id = null ) {

		// $this->wrapper_classes['background'] = 'special-class';
		// 
		// $alignment = $this->opt( $this->id.'_alignment' ) ? $this->opt( $this->id.'_alignment' ) : 'center';
		// $this->wrapper_classes['option_classes'] = "align-$alignment";

		// // WORK IN PROGRESS
		// // styles for full-width version of slider section. Mostly for placing between bands
		// // If the current section is displayed as a full width section
		// if( $this->meta['draw'] == 'area' ){
		// 	// echo "<p>BEFORE: full width section</p>";
		// 	echo "<div class='pos-rel btn-full-width' style='background: #990; padding: 10px 0; z-index: 1; width: 100%; position: absolute; left: 0; margin-top: -25px;'>";
		// }

	}
	function after_section_template( $location = '', $clone_id = null ) {
		// if( $this->meta['draw'] == 'area' ){
		// 	echo "</div>";
		// 	// echo "<p>AFTER: full width section</p>";
		// }
	}

	function section_global_markup(){
		global $global_slider_markup_printed;
		$slot_id = $this->opt( $this->id.'_style' );
		if($global_slider_markup_printed[$slot_id] == true)
			return false;

		global $sms_redux;
		global $sms_utils;

		$template_id = $sms_redux[ $this->opt( $this->id.'_style' ) ];
		$template_meta = $sms_utils->get_redux_post_meta( $template_id );
		
		if(!isset($template_id))
			return false;

		echo "\n".$template_meta['global-markup']."\n";

		// Set variable to true now that the global data has been output by this style
		$global_slider_markup_printed[$slot_id] = true;
	}

	// SECTION MARKUP - This is the function that outputs all the HTML onto the page. Put all your viewable content here.
	function section_template() {

		global $sms_redux;
		global $sms_utils;
		global $pldraft;
		$this->pldraft = $pldraft;

		// Add global markup to first instance of this section on the page.
		// This ensures that it's only loaded in if a section is on the page
		$this->section_global_markup();

		$dms_output = new smsDmsFieldFactory();
		$template_id = $dms_output->validate_template_id_by_slot_slug( $this->opt( $this->id.'_style' ), 'slider' );
		echo $dms_output->render_frontend( $template_id, $this );

	}

	function section_opts(){

		global $sms_utils;
		global $sms_redux;

    $dms_output = new smsDmsFieldFactory();
    $template_id = $dms_output->validate_template_id_by_slot_slug( $this->opt( $this->id.'_style' ), 'slider' );
    $options = $dms_output->generate_frontend_fields($template_id, $this, 'slider');

		return $options;
	}

} // Don't put code past this point.