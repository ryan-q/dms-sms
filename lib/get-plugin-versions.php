<?php

add_filter( 'query_vars', function( $qv ){
    $qv[] = 'json-get-plugins';
    return $qv;
});

add_action( 'template_redirect', function(){

  // Check if get_plugins() function exists. This is required on the front end of the
  // site, since it is in a file that is normally only loaded in the admin.
  if ( ! function_exists( 'get_plugins' ) ) {
    require_once ABSPATH . 'wp-admin/includes/plugin.php';
  }

  $input = get_query_var( 'json-get-plugins' );
  $secret = 'mastermind';

  if( ! empty( $input ) ){
    if( $secret === $input ){

      $all_plugins = get_plugins();
      $list_of_all_plugins = array();
      foreach ($all_plugins as $slug => $data) {
        $parts  = explode('/', $slug);
        $slug = $parts[0];
        $list_of_all_plugins[$slug] = array(
          'Slug' => $slug,
          'Name' => $data['Name'],
          'Version' => $data['Version'],
        );
      }
      header("HTTP/1.1 200 OK");
      wp_send_json_success( $list_of_all_plugins );
    }
    else{
      wp_send_json_error();
    }
  }

} );

