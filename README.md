# Style Management System

A plugin that works in tandem with the DMS Skeleton child theme of Pagelines DMS. This plugin provides controls for global styling settings.

* Fonts
* Colors
* Band Styles
    * Typography
    * Borders
    * Shadows
    * Backgrounds

This plugins also provides factories for:

* Buttons
* Cards
* Quotes
* Sliders
* Dividers
* Accordions
* Grids
* Lists
* Style Chunks

## Magnific Popup / Modal Boxes ##

### Youtube video ###

This allows you to open an image that will scale to fit the width of the window, and allow you to scroll up and down to view the rest. 

**Class:** popup-youtube
**Alternate Classes:** mfp-youtube

Usage: 

    <a href="image.jpg" class="open-popup-link">open popup with inline html content</a>



### Inline Content ###

**Class:** popup-inline 
**Alternate Classes:** popup-inline, mfp-inline

Usage: 

    <a href="#id_of_target" class="open-popup-link">open popup with inline html content</a>

    <div id="id_of_target">This content will display in the modal box</div>


### Image - Vertical Fit (default image popup behavior) ###

**Class:** popup-image
**Alternate Classes:** image-popup-vertical-fit, popup-fit-height

### Image - Horizontal Fit w/ vertical Scrolling ###

This allows you to open an image that will scale to fit the width of the window, and allow you to scroll up and down to view the rest. 

**Class:** image-popup-horizontal-fit
**Alternate Classes:** popup-fit-width, popup-full-width, popup-testimonial

Usage: 

    <a href="image.jpg" class="open-popup-link">open popup with inline html content</a>

## Scroll To Functionality ##

Add the class ```scrollto``` and add the ID of the element you want to scroll to. 

```<a href="#awesome_thing" class="scrollto">Scroll to awesome thing</a>```

```<div id="awesome_thing">link will scroll here</div>```