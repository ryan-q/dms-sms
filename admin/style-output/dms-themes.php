<?php

// Generate Pagelines Background Theme options to choose the bands

add_filter('pl_theme_classes', 'sms_get_dms_canvas_classes');

function sms_get_dms_canvas_classes($dms_background_themes){

  $sms_options = get_option('sms_options');
  global $sms_utils;

  // empty the default list of DMS themes
  $dms_background_themes = array();

  $i = 1;
  foreach ($sms_utils->get_band_slot_data() as $int => $slot_info) {
    $id = $slot_info['id'];
    $shade = ($slot_info['shade'] == 'dark' ? 'dark' : 'light');
    if($shade == 'dark'){
      $icon = '♟';
    } else {
      $icon = '♙';
    }
    $post_meta = $sms_utils->get_redux_post_meta($id);
    $pl_class = ($slot_info['shade'] == 'dark' ? 'pl-black' : 'pl-white');
    $slug = $slot_info['slot-slug'];
    $n = $slot_info['slot-number'];
    $dms_background_themes["sms-$slug sms-${shade} sms-band $pl_class"] = $icon.' '.$n.') '.$slot_info['title'];
    $debug_array[] = array(
      'slot-info' => $slot_info,
      'post-meta' => $post_meta,
      'shadow-blur' => $post_meta['shadow-blur'],
    );
    $i++;
  }
  // echo "<pre>\$debug_array: " . print_r($debug_array, true) . "</pre>";
  /* 
   * structure example 
   * $dms_background_themes['className'] = 'Background Theme Name';
   * 
   */
  // return $slot_info;
  return array_unique($dms_background_themes);
};

// echo "<pre>sms_get_dms_canvas_classes(): " . print_r(sms_get_dms_canvas_classes(), true) . "</pre>";