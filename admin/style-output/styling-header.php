<?php

add_action('nothing', function(){
// add_action('admin_init', function(){

	$sms_options = get_option('sms_options');
	global $sms_redux;
	global $sms_utils;

	$header_slot_list = array(
		'header-slot1',
	);

	foreach ($header_slot_list as $key) :

		$id = $sms_redux[$key];
		if(!$id)
			continue;

		if( $id ){
			$title = get_the_title($id);
			$post_edit_url = get_edit_post_link( $id );
			$meta = $sms_utils->get_redux_post_meta( $id );
		}

		$less = $meta['less'];
		if($less){
			$compiled_less = $sms_utils->compile_less( $less, "$key: <a href='$post_edit_url'>$title</a>" );

			// If the compilation was successful wrap it in comments that indicate 
			// where the CSS came from and save to database
			if($compiled_less){
				$css = "\n/* --- $key post id: $id --- */\n";
				$css .= $compiled_less;
				$css .= "/* ---------------------------------- */\n";
				$sms_utils->sms_css[$key] = $css;
			} else {
				// Clear the database entry if there's no CSS to compile.
				$sms_utils->sms_css[$key] = '';
			}

		}

	
	endforeach;

}, 10);
