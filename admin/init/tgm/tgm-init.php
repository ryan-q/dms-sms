<?php

/**
 * TGM Init Class
 */
include_once ('class-tgm-plugin-activation.php');

function starter_plugin_register_required_plugins() {

	$plugins = array(
		array(
			'name' 		=> 'Redux Framework',
			'slug' 		=> 'redux-framework',
			'required' 	=> true,
			'required' 	=> true,
		),
		array(
			'name' 		         => 'Single Value Taxonomy UI',
			'slug' 		         => 'single-value-taxonomy-ui',
			'required' 	       => true,
		),
		// array(
		// 	'name' => 'Github Updater',
		// 	'slug'               => 'github-updater',
		// 	'external_url'       => 'https://github.com/afragen/github-updater',
		// 	// 'source'             => DMS_SMS_PATH . 'lib/plugins/github-updater.zip',
		// 	'source'             => 'https://github.com/afragen/github-updater/archive/develop.zip',
		// 	'required'           => true,
		// 	'version'            => '2.7.1',
		// ),
		// array(
		// 	'name' => 'SMS Headings',
		// 	'slug'               => 'sms-headings',
		// 	'external_url'       => 'https://bitbucket.org/roadsidemultimedia/sms-headings',
		// 	'source'             => 'https://bitbucket.org/roadsidemultimedia/sms-headings/get/master.zip',
		// 	'required'           => true,
		// 	'version'            => '1.1.0',
		// ),
		// array(
		// 	'name' => 'SMS Spacer',
		// 	'slug'               => 'sms-spacer',
		// 	'external_url'       => 'https://bitbucket.org/roadsidemultimedia/sms-spacer',
		// 	'source'             => 'https://bitbucket.org/roadsidemultimedia/sms-spacer/get/master.zip',
		// 	'required'           => true,
		// 	'version'            => '1.1.0',
		// ),
		// array(
		// 	'name' => 'SMS Social Bar',
		// 	'slug'               => 'sms-social-bar',
		// 	'source'             => 'https://bitbucket.org/roadsidemultimedia/sms-social-bar/downloads/sms-social-bar.zip',
		// 	'external_url'       => 'https://bitbucket.org/roadsidemultimedia/sms-social-bar',
		// 	'required'           => false,
		// 	'version'            => '1.0.0',
		// ),
		// array(
		// 	'name' => 'SMS Master Slider',
		// 	'slug'               => 'sms-master-slider',
		// 	'source'             => 'https://bitbucket.org/roadsidemultimedia/sms-master-slider/downloads/sms-master-slider.zip',
		// 	'external_url'       => 'https://bitbucket.org/roadsidemultimedia/sms-master-slider',
		// 	'required'           => false,
		// 	'version'            => '1.0.0',
		// ),
	);

	$config = array(
		'domain'       		  => 'dms-sms',         	// Text domain - likely want to be the same as your theme.
		'default_path'    	=> '',                         	// Default absolute path to pre-packaged plugins
		'parent_menu_slug' 	=> 'plugins.php', 				// Default parent menu slug
		'parent_url_slug' 	=> 'plugins.php', 				// Default parent URL slug
		'menu'         		  => 'install-required-plugins', 	// Menu slug
		'has_notices'      	=> true,                       	// Show admin notices or not
		// 'is_automatic'    	=> true,					   	// Automatically activate plugins after installation or not
	);

	tgmpa( $plugins, $config );

}
add_action( 'tgmpa_register', 'starter_plugin_register_required_plugins' );
