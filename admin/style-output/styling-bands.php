<?php

add_action('admin_init', function(){

	$less_cache_bust = get_option('sms_less_cache_bust');
	if( $less_cache_bust || isset($_GET['compile']) ){

		$sms_options = get_option('sms_options');
		global $sms_utils;
		$selected_color_scheme_meta = $sms_utils->global_selected_color_scheme_meta;
		$band_slot_list = $sms_utils->get_band_slot_data();

		function get_link_css( $band_meta ){
			global $sms_utils;
			$type = $band_meta["link-color-type"] ?: 'custom';

			if( $type == 'global' ){

				$color_val = $band_meta["link-color-global"];
				$color_brightness_val = $band_meta["link-color-global-brightness"];
				$color = $color_val ? '@'.str_replace('def-', '', $color_val) : false;
				if( $color_brightness_val && $color_val  ) 
					$color .= "-$color_brightness_val";

				$hover_color_val = $band_meta["link-color-hover-global"];
				$hover_color_brightness_val = $band_meta["link-color-global-hover-brightness"];
				$hover_color = $hover_color_val ? '@'.str_replace('def-', '', $hover_color_val) : false;
				if( $hover_color_brightness_val && $hover_color_val  ) 
					$hover_color .= "-$hover_color_brightness_val";

			} else {

				$color = $band_meta["link-color-custom"]['color'];
				$hover_color = $band_meta["link-color-hover-custom"]['color'];

			}
			if(!$color){
				return array(
					'property' => false,
					'value' => false,
				);
			}

			return array(
				'property' => "color",
				'value' => "$color",
				'value_hover' => "$hover_color",
			);
		}


		function get_border_css( $location = 'top', $band_meta ){
			global $sms_utils;
			$width  = $band_meta["border-$location-width"] ? ( $band_meta["border-$location-width"] * 0.1).'rem' : '0.5rem';
			$type   = $band_meta["border-$location-color-type"] ?: 'custom';

			if( $band_meta["border-$location-color-type"] == 'global' ){
				$color_val = $band_meta["border-$location-color-global"];
				$color_brightness_val = $band_meta["border-$location-color-global-brightness"];
				$color = $color_val ? '@'.str_replace('def-', '', $color_val) : false;
				if( $color_brightness_val && $color_val  ) 
					$color .= "-$color_brightness_val";
			} else {
				$rgb    = $band_meta["border-$location-color"]['color'] ? $sms_utils->hex2rgb( $band_meta["border-$location-color"]['color'] ) : false;
				$alpha  = $band_meta["border-$location-color"]['alpha'] ? $band_meta["border-$location-color"]['alpha'] : false;
				$color  = "rgba({$rgb['red']},{$rgb['green']},{$rgb['blue']},{$alpha})";
			}
			if(!$color){
				return array(
					'property' => false,
					'value' => false,
				);
			}

			return array(
				'property' => "border-$location",
				'value' => "solid $width $color",
			);
		}

		$css_array = array();
		$i = 1;
		foreach ($band_slot_list as $int => $slot_info) :

			$css_selectors = new CssSelectors();
			$shade = ($slot_info['shade'] == 'dark' ? 'dark' : 'light');
			$n = $slot_info['slot-number'];
			$band_meta = $sms_utils->get_redux_post_meta( $slot_info['id'], 'sms_redux', true );
			// echo "<pre>band fields before: " . print_r($band_meta, true) . "</pre>";

			// $band_meta_w_defaults = redux_post_meta( $slot_info['id'], 'sms_redux' );
			// echo "<pre>band fields after: " . print_r($band_meta_w_defaults, true) . "</pre>";
			
			//EXAMPLE: .sms-light-band1
			$css_selector = ".sms-" . $slot_info['slot-slug'];
			
			
			$css_selectors->get($css_selector);

			// ================================================================================================================
			// TYPOGRAPHY
			// ================================================================================================================

			// $css_selectors->get($css_selector)->add( 'font-family', $band_meta['heading-font'] );


			$band_font_assignment_field_array = array(
			  'body' => '.sms-band'.$css_selector,
			  'heading--primary' => '.sms-band'.$css_selector.' .sms-heading--primary',
			  'heading--secondary' => '.sms-band'.$css_selector.' .sms-heading--secondary',
			  'heading--tertiary' => '.sms-band'.$css_selector.' .sms-heading--tertiary',
			  // 'quote' => '.sms-band'.$css_selector.' blockquote.sms-quote',
			);

			foreach ($band_font_assignment_field_array as $key => $selector) {

				$font_type = $band_meta[$key.'-font'];
				$font_family = $band_meta[$key.'-font'] ? $sms_utils->get_font_family_from_font_type( $band_meta[$key.'-font'] ) : '';
				$font_weight = $band_meta[$key.'-weight'];
				$font_size = ($key == 'body') ? false : $sms_utils->get_font_size_rem_from_key($band_meta[$key.'-size']);
				$font_color_key = $band_meta[$key.'-color'];
				$font_color_brightness = $band_meta[ $key.'-color-brightness' ];
				
				$font_color = $font_color_key ? '@'.str_replace('def-', '', $font_color_key) : false;

				if( $font_color_brightness && $font_color_key ) $font_color .= "-$font_color_brightness";

				$css_selectors->get($selector);
				$css_selectors->get($selector)->add( 'font-family', $font_family );
				$css_selectors->get($selector)->add( 'font-weight', $font_weight );
				$css_selectors->get($selector)->add( 'text-align', $band_meta[$key.'-text-align'] );


				$letter_spacing = $band_meta[$key.'-letter-spacing'] ? ($band_meta[$key.'-letter-spacing'] * .01) . 'em' : false;
				$css_selectors->get($selector)->add( 'letter-spacing', $letter_spacing );

				$css_selectors->get($selector)->add( 'font-size', $font_size );

				$font_line_height_multiplier = $band_meta[$key.'-line-height'];

				if($font_line_height_multiplier){
					$font_line_height = $font_line_height_multiplier;
					$css_selectors->get($selector)->add( 'line-height', $font_line_height );
				}

				if( $band_meta[$key.'-text-transform'] == 'small-caps'){
					$css_selectors->get($selector)->add( 'font-variant', $band_meta[$key.'-text-transform'] );
				} else {
					$css_selectors->get($selector)->add( 'text-transform', $band_meta[$key.'-text-transform'] );
				}

				if($font_color){
					$css_selectors->get($selector)->add( 'color', $font_color );
				}

			}

			// ================================================================================================================
			// LINK COLOR
			// ================================================================================================================

			if( $band_meta['link-color-enabled'] == true ){

				$link_color_css = get_link_css($band_meta);
				$css_selectors->get("#site $css_selector a, #site $css_selector a:visited")->add($link_color_css['property'], $link_color_css['value']);

				if( $link_color_css['value_hover'] ){
					$hover = $link_color_css['value_hover'];
				} else {
					$color = $link_color_css['value'];
					$light = "mix($color, #fff, 60%)";
					$dark = "mix($color, #000, 60%)";
					$hover = "contrast($color, $light, $dark, 50%);";
				}
				
				$css_selectors->get("#site $css_selector a:hover")->add($link_color_css['property'], $hover);

			}

			// ================================================================================================================
			// BORDERS
			// ================================================================================================================

			if( $band_meta['borders-enabled'] == true ){
				
				if($band_meta['border-top-enabled'] == true){
					$border_top_css = get_border_css('top', $band_meta);
					$css_selectors->get($css_selector.' .pl-area-wrap')->add($border_top_css['property'], $border_top_css['value']);
				}
				
				if($band_meta['border-bottom-enabled'] == true){
					$border_bottom_css = get_border_css('bottom', $band_meta);
					$css_selectors->get($css_selector.' .pl-area-wrap')->add($border_bottom_css['property'], $border_bottom_css['value']);
				}

			}

			// ================================================================================================================
			// SHADOWS
			// ================================================================================================================

			if( $band_meta['shadow-enabled'] == true ){

				$shadow_blur      = $band_meta['shadow-blur'] ? ($band_meta['shadow-blur']*0.1).'rem' : '1rem';
				$shadow_location  = $band_meta['shadow-location'] ?: 'both';
				
				$shadow_color_rgb = $sms_utils->hex2rgb( $band_meta['shadow-color'] ) ?: array('red' => '0','blue' => '0','green' => '0');
				$shadow_opacity   = $band_meta['shadow-opacity'] ? ($band_meta['shadow-opacity'] * 0.01) : '0.4';

				$sh = array(
					'inset' => $band_meta['shadow-inset'] == 'outset' ? '' : 'inset',
					'v_offset' => $shadow_blur,
					'blur' => $shadow_blur, 
					'spread' => '-'.$shadow_blur,
					'color' => "rgba({$shadow_color_rgb['red']},{$shadow_color_rgb['green']},{$shadow_color_rgb['blue']},{$shadow_opacity})",
				);

				$band_shadow_property = '';
				if( $shadow_location == 'top' || $shadow_location == 'both' ){
					$band_shadow_property .= $sh['inset'].' 0 '.$sh['v_offset'].' '.$sh['blur'].' '.$sh['spread'].' '.$sh['color'];
				}

				if( $shadow_location == 'both' )
					$band_shadow_property .= ', ';
				
				if( $shadow_location == 'bottom' || $shadow_location == 'both'){
					$sh['v_offset'] = '-'.$shadow_blur;
					$band_shadow_property .= $sh['inset'].' 0 '.$sh['v_offset'].' '.$sh['blur'].' '.$sh['spread'].' '.$sh['color'];
				}

				if( $band_meta['shadow-inset'] == 'outset' ){
					$css_selectors->get($css_selector.' .pl-area-wrap')->add('position', 'relative');
				}

				$css_selectors->get($css_selector.' .pl-area-wrap')->add('box-shadow', $band_shadow_property);
				

			}



			// ================================================================================================================
			// BACKGROUNDS
			// ================================================================================================================
			// 
			// 
			// echo "<div style='background: rgba(0,0,0,.2);'><h3>".$slot_info['slot-slug'].": ".$slot_info['id']."</h3><pre>" . print_r($band_meta['background-type'], true) . "</pre></div>";
			if( $band_meta['background-type'] == 'background-color' ){

				//////////////////////////////////////////////////////////////
				// Background Color
				$css_selectors->get($css_selector)->add('background-color', $band_meta['background-color']);

			} elseif( $band_meta['background-type'] == 'background-color-global' ){

				//////////////////////////////////////////////////////////////
				// Background Color (Global color)
				
				$global_color_val = $band_meta['background-color-global'];
				$global_color_brightness_val = $band_meta['background-color-brightness-global'];
				$global_color_val = $global_color_val ? '@'.str_replace('def-', '', $global_color_val) : false;

				if( $global_color_brightness_val ) $global_color_val .= "-$global_color_brightness_val";

				$css_selectors->get($css_selector)->add('background-color', $global_color_val);

			} elseif( $band_meta['background-type'] == 'background-image' ){

				//////////////////////////////////////////////////////////////
				// Background Texture & Color
				$property = $band_meta['background-image'];
				$css_selectors->get($css_selector)->add('background-repeat', $property['background-repeat']);
				$css_selectors->get($css_selector)->add('background-attachment', $property['background-attachment']);
				$css_selectors->get($css_selector)->add('background-position', $property['background-position']);

				if( $property['background-image'] )
					$css_selectors->get($css_selector)->add('background-image', "url('". $property['background-image'] ."')");
				
				if( $property['background-attachment'] == 'fixed' ){
					$mobile_bg = ''.
'@media only screen and (max-width: 768px) {'.
'  &{'.
'    background-size: cover !important;'.
'    background-attachment: initial !important;'.
'  }'.
'}';
					$band_meta['less'] = $mobile_bg . "\r" . $band_meta['less'];

				}

				// Fix for DMS class that automatically makes the background size set to fill
				if( !$property['background-size'] )
					$css_selectors->get($css_selector)->add('background-size', 'auto auto !important');


				// Color overlay
				$enabled_setting = $band_meta['background-image--overlay-enabled'];
				$opacity_setting = $band_meta['background-image--overlay-opacity'];
				$color_setting = $band_meta['background-image--overlay-color'];
				
				$color_rgb = ( $color_setting ? $sms_utils->hex2rgb($color_setting) : false );
				$opacity = ( $opacity_setting ? ($opacity_setting / 100) : 1 );

				$color_rgba = 'rgba('.$color_rgb['red'].','.$color_rgb['green'].','.$color_rgb['blue'].','.$opacity.')';

				if( $enabled_setting == true && $color_rgb ){
					$css_selectors->get($css_selector.' .pl-area-wrap')->add('background', $color_rgba);
				}


			} elseif( $band_meta['background-type'] == 'background-library' ){

				//////////////////////////////////////////////////////////////
				// Background Library
				$property = $band_meta['background-library'];
				if( $property ){
					$css_selectors->get($css_selector)->add('background-image', "url('". $property ."')");
					$css_selectors->get($css_selector)->add('background-repeat', "repeat");
					$css_selectors->get($css_selector)->add('background-size', "auto auto !important");
				}

			} elseif( $band_meta['background-type'] == 'background-gradient' ){

				//////////////////////////////////////////////////////////////
				// Background Gradient
				//////////////////////////////////////////////////////////////
				$property = $band_meta['background-gradient'];

				if( $property ){

					$property['from'];
					$property['to'];

					$css_selectors->get($css_selector)->add('background-color', $property['from']);

					$gradient_less = sprintf( ".gradient(%s, %s);", $property['from'], $property['to'] );
					$band_meta['less'] = $gradient_less . "\r" . $band_meta['less'];

					// $sms_utils->admin_notices['updated'][] = "<pre>".print_r($css_selector, true)."</pre>";

				}

			}


			
			// Avoid outputting duplicate band styles
			if( array_key_exists($slot_info['id'], $css_array) == false ){
				$css_array[ $slot_info['id'] ] = array(
					'slot' => $slot_info['slot-slug'],
					'css' => $css_selectors->get_css(),
					'custom_css' => $band_meta['less'],
				);
			} else {
				// This slot has already had it's CSS processed.
				// $sms_utils->admin_notices['updated'][] = "Already processed: id: ".$slot_info['id']. " / Shade: ".$shade." / Number: ".$n;
			}
			/* // CLOSE
			 * ///////////////////////////////////////////////////////
			 */
			$i++;

		endforeach;

		// Display preview of less that will be compiled.
		// $sms_utils->admin_notices['updated'][] = "<pre>".print_r($css_array, true)."</pre>";
		
		// Process the previously collected info from $css_array
		$css_string = '';
		foreach ($css_array as $id => $value) {
			$css_selector = ".sms-" . $value['slot'];
			$css_string .= $sms_utils->dev_mode ? "\n\n/* --- Band ".$value['slot']." | ID: $id --- */\n\n" : '';
			$css_string .= $sms_utils->compile_less( $value['css'], "styling-bands.php | Post ID: $id" );
			
			if($value['custom_css']){
				$css_guard = $css_selector."{".$value['custom_css']."}";
				$css_string .= $sms_utils->dev_mode ? "\n/*** Custom CSS ***/\n" : '';
				$css_string .= $sms_utils->compile_less( $css_guard, "Post ID: $id" );
			}
		}

		// If the compilation was successful wrap it in comments that indicate 
		// where the CSS came from and save to database
		if( $css_string ){
			$css = $sms_utils->dev_mode ? "\n\n/* ========== Band Styles ========================= */\n\n" : '';
			$css .= $css_string;
			$css .= $sms_utils->dev_mode ? "\n\n/* ========== End Band Styles ========================= */\n\n" : '';
			$sms_utils->sms_css['band-styles'] = $css;
		} else {
			// Clear the database entry if there's no CSS to compile.
			$sms_utils->sms_css['band-styles'] = '';
		}
		// echo "<pre>\$css: " . print_r($css, true) . "</pre>";

		// Clear legacy LESS DB entry
		$sms_utils->sms_less['band-styles'] = '';

	}

});
